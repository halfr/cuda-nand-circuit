#include <string>

#define uint32_t unsigned int

typedef uint32_t pin_t;

#pragma pack()
struct NandDesc
{
    // Input pins
    pin_t a, b;
    // Output
    pin_t o;
};

#if 0
struct IODesc
{
    std::string name;
    pin_t start;
    size_t busSize;
};

struct Chip
{
    std::vector<IODesc> inputs;
    std::vector<IODesc> outputs;
    std::vector<
};
#endif
