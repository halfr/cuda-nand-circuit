CHIPS := AND NOT OR XOR AND16 NOT16 XOR16 NAND16 MUXER16 TOXORNOTTOX LOVEANDHATE INC1 INC8 HALF_ADDER FULL_ADDER ALU ADDER INC2

all: $(CHIPS)

build:
	mkdir -p build

ifdef HOST
define make-chip
$1: nand.cu nand.h chips/$1_netlist.cpp chips/$1_netlist.h | build
	g++ -std=c++11 -xc++ ./nand.cu ./chips/$1_netlist.cpp -I. -Ichips -include $1_netlist.h -o build/$1
endef
else
define make-chip
$1: nand.cu nand.h chips/$1_netlist.cpp chips/$1_netlist.h | build
	nvcc ./nand.cu ./chips/$1_netlist.cpp -I. -Ichips -include $1_netlist.h -o build/$1 -ccbin=gcc-4.8.5
endef
endif

$(foreach c,$(CHIPS),$(eval $(call make-chip,$c)))

repro:
	nvcc ./repro.cu -o $@ -arch compute_52
