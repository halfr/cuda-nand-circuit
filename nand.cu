#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <chrono>

#include "nand.h"

#ifdef __CUDACC__
#define checkCuda(ret) { cudaAssert((ret), __FILE__, __LINE__); }
inline void cudaAssert(cudaError_t code, const char *file, int line, bool abort=false)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr, "cudaAssert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void nand(const NandDesc* netlist, bool* wires)
{
    int nandId = blockIdx.x * blockDim.x + threadIdx.x;
    if (nandId >= NETLIST_NANDCOUNT)
        return;

    const NandDesc desc = netlist[nandId];
    bool in_a = wires[desc.a];
    bool in_b = wires[desc.b];
    bool out = !(in_a & in_b);
    wires[desc.o] = out;
//printf("#%d - !([%d] & [%d]) = [%d]\n", nandId, nandId, nandId, nandId);
//printf("#%d - !(%d & %d) = %d\n", nandId, nandId, nandId, nandId);
}
#else
static void nand(const NandDesc *netlist, size_t nandId, bool* wires)
{
    const NandDesc desc = netlist[nandId];
    bool in_a = wires[desc.a];
    bool in_b = wires[desc.b];
    bool out = !(in_a & in_b);
    wires[desc.o] = out;
}
#endif

static void writeWire(bool* wires, size_t start, size_t busSize, uint32_t value)
{
    for (int i = start; i < start + busSize; ++i)
    {
        wires[i] = value & 1;
        value >>= 1;
    }
}

static uint32_t readWire(bool* wires, size_t start, size_t busSize)
{
    uint32_t ret = 0;

    for (int i = 0; i < busSize; ++i)
    {
        ret <<= 1;
        ret |= static_cast<uint32_t>(wires[start + busSize - i - 1]);
    }

    return ret;
}

static void readInput(bool* wires, int argc, char** argv)
{
    for (int i = 1; i < argc; ++i) {
        if (strcmp("-w", argv[i]))
            continue;

        uint32_t were = static_cast<uint32_t>(strtoll(argv[i+1], NULL, 16));
        uint32_t busSize = static_cast<uint32_t>(strtoll(argv[i+2], NULL, 16));
        uint32_t what;
        sscanf(argv[i+3], "%x\n", &what);
        writeWire(wires, were, busSize, what);

        i += 2;
    }
}

static void printOutput(bool *wires, int argc, char** argv)
{
    for (int i = 1; i < argc; ++i) {
        if (strcmp("-r", argv[i]))
            continue;

        uint32_t were = static_cast<uint32_t>(strtoll(argv[i+1], NULL, 16));
        uint32_t busSize = static_cast<uint32_t>(strtoll(argv[i+2], NULL, 16));
        char* what = argv[i+3];
        uint32_t out = readWire(wires, were, busSize);
        printf("%s: 0x%04x\n", what, out);

        i += 2;
    }
}

extern const NandDesc h_netlist[NETLIST_NANDCOUNT];
extern const size_t wiresCount;
extern const size_t ioDiameter;
extern const size_t maxBreadth;
extern const char* helpString;

int main(int argc, char** argv)
{
    if (argc == 1 || (argc - 1) % 4 != 0)
    {
        puts("usage: [-w 0xwhere 0xbusSize 0xwhat]* [-r 0xwhere 0xbusSize what]*");
        printf("%s", helpString);
        return 1;
    }

    float elapsedTime = 0;

    const size_t wiresSize = wiresCount * sizeof (bool);
    bool* h_wires;
#ifdef __CUDACC__
    checkCuda(cudaMallocHost(&h_wires, wiresSize));
#else
    h_wires = new bool[NETLIST_NANDCOUNT];
#endif
    memset(h_wires, 0, wiresSize);

    h_wires[0] = 0; // Wire 0 is always 0
    h_wires[1] = 1; // Wire 1 is always 1

    readInput(h_wires, argc, argv);

#ifdef __CUDACC__
    bool* d_wires;
    checkCuda(cudaMalloc(&d_wires, wiresSize));
    checkCuda(cudaMemcpy(d_wires, h_wires, wiresSize, cudaMemcpyDefault));

    NandDesc *d_netlist;
    checkCuda(cudaMalloc(&d_netlist, NETLIST_NANDCOUNT * sizeof(NandDesc)));
    checkCuda(cudaMemcpy(d_netlist, h_netlist, NETLIST_NANDCOUNT * sizeof(NandDesc), cudaMemcpyHostToDevice));

    dim3 gridSize(NETLIST_NANDCOUNT / 1024 + 1);
    dim3 blockSize(1024);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    for (int i = 0; i < ioDiameter; ++i) {
        nand<<<gridSize, 1024>>>(d_netlist, d_wires, 1024);
        checkCuda(cudaDeviceSynchronize()); // get errors from the kernel
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&milliseconds, start, stop);

    // Bring back the data from the GPU
    checkCuda(cudaMemcpy(h_wires, d_wires, wiresSize, cudaMemcpyDeviceToHost));

    cudaFree(d_wires);
    cudaFree(d_netlist);
#else
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::system_clock::now();
    for (size_t i = 0; i < ioDiameter; ++i) {
        for (size_t n = 0; n < NETLIST_NANDCOUNT; ++n) {
            nand(h_netlist, n, h_wires);
        }
    }
    end = std::chrono::system_clock::now();

    std::chrono::duration<float, std::milli> duration = end - start;
    elapsedTime = duration.count();
#endif

    printOutput(h_wires, argc, argv);
    printf("Computation took: %fms\n", elapsedTime);

    return 0;
}
